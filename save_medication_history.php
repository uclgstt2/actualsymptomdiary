<?php

session_start ();

include ('db_conn.php');

$db = new DBconn();

$conn = $db->connect();


if (isset ($_SESSION['patID'])){
    $patID = $_SESSION['patID'];
}


//CHEMOTHERAPY
if(isset ($_SESSION['chemotherapy_date_time'])){
   $chemotherapy_date_time = $_SESSION['chemotherapy_date_time'] ;
}

if(isset ($_SESSION['chemotherapy_no_of_tablets'])){
    $chemotherapy_no_of_tablets = $_SESSION['chemotherapy_no_of_tablets'];
}

/*
 *
if(isset ($_SESSION['chemotherapy_date_time'])) {
  echo($_SESSION['chemotherapy_date_time'] . '<br>');
}

if(isset ($_SESSION['chemotherapy_no_of_tablets'])){
  echo ($_SESSION['chemotherapy_no_of_tablets'] . '<br>');
}
*/

//ANTISICKNESS
if(isset ($_SESSION['antisickness_date_time'])){
    $antisickness_date_time = $_SESSION['antisickness_date_time'] ;
}

if(isset ($_SESSION['antisickness_no_of_tablets'])){
    $antisickness_no_of_tablets = $_SESSION['antisickness_no_of_tablets'];
}


//BOWEL
if(isset ($_SESSION['bowel_date_time'])){
    $bowel_date_time = $_SESSION['bowel_date_time'];
}

if(isset ($_SESSION['bowel_no_of_tablets'])){
    $bowel_no_of_tablets = $_SESSION['bowel_no_of_tablets'];
}



//CAPECITABINE
if(isset ($_SESSION['capecitabine_date_time'])){
    $capecitabine_date_time = $_SESSION['capecitabine_date_time'];
}

if(isset ($_SESSION['capecitabine_no_of_tablets'])){
    $capecitabine_no_of_tablets = $_SESSION['capecitabine_no_of_tablets'];
}


//METOCLOPROMIDE
if(isset ($_SESSION['metoclopromide_date_time'])){
    $metoclopromide_date_time = $_SESSION['metoclopromide_date_time'];
}

if(isset ($_SESSION['metoclopromide_no_of_tablets'])){
    $metoclopromide_no_of_tablets = $_SESSION['metoclopromide_no_of_tablets'];
}


//DOMPERIDONE
if(isset ($_SESSION['domperidone_date_time'])){
    $domperidone_date_time = $_SESSION['domperidone_date_time'];
}

if(isset ($_SESSION['domperidone_no_of_tablets'])){
    $domperidone_no_of_tablets = $_SESSION['domperidone_no_of_tablets'];
}


//ONDANSETRON
if(isset ($_SESSION['ondansetron_date_time'])){
    $ondansetron_date_time = $_SESSION['ondansetron_date_time'];
}

if(isset ($_SESSION['ondansetron_no_of_tablets'])){
    $ondansetron_no_of_tablets = $_SESSION['ondansetron_no_of_tablets'];
}


//MOVICOL SACHET
if(isset ($_SESSION['movicolsachet_date_time'])){
    $movicolsachet_date_time = $_SESSION['movicolsachet_date_time'];
}

if(isset ($_SESSION['movicolsachet_no_of_tablets'])){
    $movicolsachet_no_of_tablets = $_SESSION['movicolsachet_no_of_tablets'] ;
}


//LANSOPRAZOLE
if(isset ($_SESSION['lansoprazole_date_time'])){
    $lansoprazole_date_time = $_SESSION['lansoprazolen_date_time'];
}

if(isset ($_SESSION['lansoprazole_no_of_tablets'])){
  $lansoprazole_no_of_tablets = $lansoprazole_no_of_tablets =  $_SESSION['lansoprazole_no_of_tablets'];
}


//OMEPRAZOLE
if(isset ($_SESSION['omeprazole_date_time'])){
    $omeprazole_date_time = $_SESSION['omeprazole_date_time'];
}

if(isset ($_SESSION['omeprazole_no_of_tablets'])){
    $omeprazole_no_of_tablets = $_SESSION['omeprazole_no_of_tablets'];
}



//GAVISON
if(isset ($_SESSION['gavison_date_time'])){
    $gavison_date_time  = $_SESSION['gavison_date_time'];
}

if(isset ($_SESSION['gavison_no_of_tablets'])){
    $gavison_no_of_tablets = $_SESSION['gavison_no_of_tablets'] ;
}



//ORAL MORPHINE
if(isset ($_SESSION['oralmorphine_date_time'])){
    $oralmorphine_date_time = $_SESSION['oralmorphine_date_time'] ;
}

if(isset ($_SESSION['oralmorphine_no_of_tablets'])){
    $oralmorphine_no_of_tablets = $_SESSION['oralmorphine_no_of_tablets'] ;
}



//MORPHINE TABLETS
if(isset ($_SESSION['morphinetablets_date_time'])){
    $morphinetablets_date_time = $_SESSION['morphinetablets_date_time'];
}

if(isset ($_POST['morphinetablets_no_of_tablets'])){
    $morphinetablets_no_of_tablets = $_SESSION['morphinetablets_no_of_tablets'];
}


//PARACETAMOL
if(isset ($_SESSION['paracetamol_date_time'])){
    $paracetamol_date_time = $_SESSION['paracetamol_date_time'];
}

if(isset ($_SESSION['paracetamol_no_of_tablets'])){
    $paracetamol_no_of_tablets = $_SESSION['paracetamol_no_of_tablets'];
}



//CYCLIZINE
if(isset ($_SESSION['cyclizine_date_time'])){
    $cyclizine_date_time = $_SESSION['cyclizine_date_time'];
}

if(isset ($_SESSION['cyclizine_no_of_tablets'])){
    $cyclizine_no_of_tablets = $_SESSION['cyclizine_no_of_tablets'];
}



//DEXAMETHAZONE
if(isset ($_SESSION['dexamethazone_date_time'])){
    $dexamethazone_date_time = $_SESSION['dexamethazone_date_time'];
}

if(isset ($_SESSION['dexamethazone_no_of_tablets'])){
    $dexamethazone_no_of_tablets = $_SESSION['dexamethazone_no_of_tablets'];
}

/*
 * SESSION['SYMP'][LASKNDLAKS]
 * SESSION['HOSP'][KANSLCNASCL]
 * SESSION['MED'][ASKJDNASJ]
 */


$sql = <<<EOD
INSERT INTO medicationhistory(
  chemotherapy_date_time
, chemotherapy_no_of_tablets
, antisickness_date_time
, antisickness_no_of_tablets
, bowel_date_time
, bowel_no_of_tablets
, capecitabine_date_time
, capecitabine_no_of_tablets
, metoclopromide_date_time
, metoclopromide_no_of_tablets
, domperidone_date_time
, domperidone_no_of_tablets
, ondansetron_date_time
, ondansetron_no_of_tablets
, movicolsachet_date_time
, movicolsachet_no_of_tablets
, lansoprazole_date_time
, lansoprazole_no_of_tablets
, omeprazole_date_time
, omeprazole_no_of_tablets
, gavison_date_time
, gavison_no_of_tablets
, oralmorphine_date_time
, oralmorphine_no_of_tablets
, morphinetablets_date_time
, morphinetablets_no_of_tablets
, paracetamol_date_time
, paracetamol_no_of_tablets
, cyclizine_date_time
, cyclizine_no_of_tablets
, dexamethazone_date_time
, dexamethazone_no_of_tablets
, patID
    )
    VALUES (
      $chemotherapy_date_time
    , $chemotherapy_no_of_tablets
    , $antisickness_date_time
    , $antisickness_no_of_tablets
    , $bowel_date_time
    , $bowel_no_of_tablets
    , $capecitabine_date_time
    , $capecitabine_no_of_tablets
    , $metoclopromide_date_time
    , $metoclopromide_no_of_tablets
    , $domperidone_date_time
    , $domperidone_no_of_tablets
    , $ondansetron_date_time
    , $ondansetron_no_of_tablets
    , $movicolsachet_date_time
    , $movicolsachet_no_of_tablets
    , $lansoprazole_date_time
    , $lansoprazole_no_of_tablets
    , $omeprazole_date_time
    , $omeprazole_no_of_tablets
    , $gavison_date_time
    , $gavison_no_of_tablets
    , $oralmorphine_date_time
    , $oralmorphine_no_of_tablets
    , $morphinetablets_date_time
    , $morphinetablets_no_of_tablets
    , $paracetamol_date_time
    , $paracetamol_no_of_tablets
    , $cyclizine_date_time
    , $cyclizine_no_of_tablets
    , $dexamethazone_date_time
    , $dexamethazone_no_of_tablets

    , $patID
    )";

EOD;

$result = $conn->query($sql);

//$aary = $result->fetch_all(MYSQLI_ASSOC);


echo "<script language=javascript>";
    echo "window.location = 'SaveMedData.html'";
    echo "</script>";


?>
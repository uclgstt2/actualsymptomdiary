
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Navbar Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="dist/css/navbar.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <?php

session_start();


function endsession()
{
    $_SESSION = array();
    session_destroy();
    session_unset();
    header("Location:hospital_history1.php");
    die();
}
?>

<div class="container">

    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Hospital History</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">

                </ul>
                <ul class="nav navbar-nav navbar-right">

                </ul>
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>

    <!-- Main component for a primary marketing message or call to action -->
    <form >
        <label>Did you call the Hospital today?</label>

        <br><br>
        
        <button type="button" class="btn btn-success" style="margin-right: 5px" onclick="show()">Yes</button>

        <form action="hospital_history2.html">

        <button onclick="location.href = 'hospital_history2.php';" type="button" href="hospital_history2.html" class="btn btn-danger">No and Continue</button>




        <br><br>

        <div class = "form-group">
               
                <input id="yestext1" style="display:none" type="text" class="form-control" name="yestext1" placeholder="Please provide us with some details" required="true" >
                
            </div>

            <button id="yestext2" style="display:none" type="button" class="btn btn-success" onclick="Storehospinfo()" >Continue</button>

                <br>

            <button onclick="location.href = 'prototype/homescreen.html';" type="button" class="btn btn-danger">Back</button>
        

</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="dist/js/bootstrap.min.js"></script>
<script>

function Storehospinfo()
    {
        var calldetails = $("#yestext1").val();
        

        var datastring = "yestext1="+calldetails;


        $.post("hospback1.php", datastring,function(data)
        {

            //alert(data);

            if(data == "success"){

               //console.log("makaskjdk");
               window.location.href = "hospital_history2.php";
                
            }
            else
            {
                alert("fail weired");
                
                $('.success').fadeIn(200).show().delay(2000).fadeOut(200);
                setTimeout(function() {
                    location.reload();
                },3000);
            }



        }).fail(function(){
            alert("something wrong");
            
        });
    }

    function show()
    {
        $('#yestext1').fadeIn(200).show();
        $('#yestext2').fadeIn(200).show();
    }
    
</script>
</body>
</html>

<?php
/**
 * Created by PhpStorm.
 * User: Shamail
 * Date: 2/26/2015
 * Time: 9:31 AM
 */

$med = $_GET['med'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <link rel="stylesheet" type="text/css" href="./jquery.datetimepicker.css"/>

    <title>Navbar Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="dist/css/navbar.css" rel="stylesheet">
    <link href="dist/css/jquery.datetimepicker.css" rel="stylesheet" TYPE="TEXT/CSS">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>



<div class="container">

    <!-- Static navbar -->
    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <a class="navbar-brand" href="prototype/homescreen.html">Medicine Log</a>

        </div><!--/.container-fluid -->
    </nav>

    <ol class="breadcrumb">
        <li><a href="prototype/homescreen.html">Home</a></li>
        <li><a href="medication_history.php">Medications</a></li>
        <li class="active"><?php echo $med?></li>

    </ol>


    <article>
        <header>

            <h1>
                <center>Medication History</center>
            </h1>
        </header>

        <center>
            <?php



            switch($med){
                case "movicol":
                    $med_name="$med sachets";
                    break;

                case "morphine_tablets":
                    $med_name="morphine tablets";
                    break;

                default: $med_name = "$med tablets";
                    break;
             }
                echo "<p>When did you take your $med_name?</p>";
            ?>



            <form action="medication_history.php" method="post">

                <?php
                $med_date_time = $med."_date_time";
                  echo "<input type=\"text\" name=\"$med_date_time\" id=\"datetimepicker\"/><br><br>";

                ?>


                <p>How many did you take?</p>



                <?php
                    $med_no_of_tablets = $med."_no_of_tablets";
                    echo "<select name=\"$med_no_of_tablets\">";

                    for($num = 0; $num < 6; $num ++) {
                        echo ("<option value=" . $num . ">$num</option >");
                    }
                    echo "</select>";
                    ?>



                <!-- 		<a href="medication_history.php" class="homepagebegin">Submit</a> -->


                <br>
                <br>
                <button class="btn btn-primary btn-lg" type="submit">Submit</button>
            </form>

        </center>
        <footer>

            <p>


            <center>From Guy's and St Thomas' Hospital</center>

            <div style="float: right;">

                <a href="apphelp.html" class="btn"><button class="btn btn-lg btn-warning">App Info</button></a>
            </div>

            </p>

        </footer>
</div>


</body>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="dist/js/bootstrap.min.js"></script>

<script src="./jquery.datetimepicker.js"></script>


<script>

    $('#datetimepicker').datetimepicker({
        dayOfWeekStart : 1,
        lang:'en',
        disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
        startDate:	'2015/01/05'
    });
    $('#datetimepicker').datetimepicker({value:'2015/04/15 05:03',step:10});




</script>
</html>


<?php

session_start ();

$format = 'Y/m/d H:i';
//$date = DateTime::createFromFormat($format, '2009-02-15 15:16:17');
//2015/04/15 05:03

  $chemotherapy_date_time='NULL';
  $chemotherapy_no_of_tablets=0;
  $antisickness_date_time='NULL';
  $antisickness_no_of_tablets=0;
  $bowel_date_time='NULL';
  $bowel_no_of_tablets=0;
  $capecitabine_date_time='NULL';
  $capecitabine_no_of_tablets=0;
  $metoclopromide_date_time='NULL';
  $metoclopromide_no_of_tablets=0;
  $domperidone_date_time='NULL';
  $domperidone_no_of_tablets=0;
  $ondansetron_date_time='NULL';
  $ondansetron_no_of_tablets=0;
  $movicolsachet_date_time='NULL';
  $movicolsachet_no_of_tablets=0;
  $lansoprazole_date_time='NULL';
  $lansoprazole_no_of_tablets=0;
  $omeprazole_date_time='NULL';
  $omeprazole_no_of_tablets=0;
  $gavison_date_time='NULL';
  $gavison_no_of_tablets=0;
  $oralmorphine_date_time='NULL';
  $oralmorphine_no_of_tablets=0;
  $morphinetablets_date_time='NULL';
  $morphinetablets_no_of_tablets=0;
  $paracetamol_date_time='NULL';
  $paracetamol_no_of_tablets=0;
  $cyclizine_date_time='NULL';
  $cyclizine_no_of_tablets=0;
  $dexamethazone_date_time='NULL';
  $dexamethazone_no_of_tablets=0;


include ('db_conn.php');

$db = new DBconn();

$conn = $db->connect();


if (isset ($_SESSION['patID'])){
    $patID = $_SESSION['patID'];
}


//CHEMOTHERAPY
if(isset ($_SESSION['chemotherapy_date_time'])){
    $chemotherapy_date_time = DateTime::createFromFormat($format,$_SESSION['chemotherapy_date_time']);
    $chemotherapy_date_time = $chemotherapy_date_time->format($format);
    $chemotherapy_date_time = "'$chemotherapy_date_time'";
}

if(isset ($_SESSION['chemotherapy_no_of_tablets'])){
    $chemotherapy_no_of_tablets = $_SESSION['chemotherapy_no_of_tablets'];

}

/*
 *
if(isset ($_SESSION['chemotherapy_date_time'])) {
  echo($_SESSION['chemotherapy_date_time'] . '<br>');
}

if(isset ($_SESSION['chemotherapy_no_of_tablets'])){
  echo ($_SESSION['chemotherapy_no_of_tablets'] . '<br>');
}
*/

//ANTISICKNESS
if(isset ($_SESSION['antisickness_date_time'])){
    $antisickness_date_time = DateTime::createFromFormat($format, $_SESSION['antisickness_date_time']);
    $antisickness_date_time = $antisickness_date_time->format($format);
    $antisickness_date_time = "'$antisickness_date_time'";
}

if(isset ($_SESSION['antisickness_no_of_tablets'])){
    $antisickness_no_of_tablets = $_SESSION['antisickness_no_of_tablets'];
}


//BOWEL
if(isset ($_SESSION['bowel_date_time'])){
    $bowel_date_time = DateTime::createFromFormat($format,$_SESSION['bowel_date_time']);
    $bowel_date_time = $bowel_date_time->format($format);
    $bowel_date_time = "'$bowel_date_time'";
}

if(isset ($_SESSION['bowel_no_of_tablets'])){
    $bowel_no_of_tablets = $_SESSION['bowel_no_of_tablets'];
}



//CAPECITABINE
if(isset ($_SESSION['capecitabine_date_time'])){
    $capecitabine_date_time = DateTime::createFromFormat($format,$_SESSION['capecitabine_date_time']);
    $capecitabine_date_time = $capecitabine_date_time->format($format);
    $capecitabine_date_time = "'$capecitabine_date_time'";
}

if(isset ($_SESSION['capecitabine_no_of_tablets'])){
    $capecitabine_no_of_tablets = $_SESSION['capecitabine_no_of_tablets'];
}


//METOCLOPROMIDE
if(isset ($_SESSION['metoclopromide_date_time'])){
    $metoclopromide_date_time = DateTime::createFromFormat($format,$_SESSION['metoclopromide_date_time']);
    $metoclopromide_date_time = $metoclopromide_date_time->format($format);
    $metoclopromide_date_time = "'$metoclopromide_date_time'";
}

if(isset ($_SESSION['metoclopromide_no_of_tablets'])){
    $metoclopromide_no_of_tablets = $_SESSION['metoclopromide_no_of_tablets'];
}


//DOMPERIDONE
if(isset ($_SESSION['domperidone_date_time'])){
    $domperidone_date_time = DateTime::createFromFormat($format,$_SESSION['domperidone_date_time']);
    $domperidone_date_time = $domperidone_date_time->format($format);
    $domperidone_date_time = "'$domperidone_date_time'";
}

if(isset ($_SESSION['domperidone_no_of_tablets'])){
    $domperidone_no_of_tablets = $_SESSION['domperidone_no_of_tablets'];
}


//ONDANSETRON
if(isset ($_SESSION['ondansetron_date_time'])){
    $ondansetron_date_time = DateTime::createFromFormat($format,$_SESSION['ondansetron_date_time']);
    $ondansetron_date_time = $ondansetron_date_time->format($format);
    $ondansetron_date_time = "'$ondansetron_date_time'";
}

if(isset ($_SESSION['ondansetron_no_of_tablets'])){
    $ondansetron_no_of_tablets = $_SESSION['ondansetron_no_of_tablets'];
}


//MOVICOL SACHET
if(isset ($_SESSION['movicolsachet_date_time'])){
    $movicolsachet_date_time = DateTime::createFromFormat($format, $_SESSION['movicolsachet_date_time']);
    $movicolsachet_date_time = $movicolsachet_date_time->format($format);
    $movicolsachet_date_time = "'$movicolsachet_date_time'";
}

if(isset ($_SESSION['movicolsachet_no_of_tablets'])){
    $movicolsachet_no_of_tablets = $_SESSION['movicolsachet_no_of_tablets'] ;
}


//LANSOPRAZOLE
if(isset ($_SESSION['lansoprazole_date_time'])){
    $lansoprazole_date_time = DateTime::createFromFormat($format, $_SESSION['lansoprazolen_date_time']);
    $lansoprazole_date_time = $lansoprazole_date_time->format($format);
    $lansoprazole_date_time = "'$lansoprazole_date_time'";
}

if(isset ($_SESSION['lansoprazole_no_of_tablets'])){
    $lansoprazole_no_of_tablets = $lansoprazole_no_of_tablets =  $_SESSION['lansoprazole_no_of_tablets'];
}


//OMEPRAZOLE
if(isset ($_SESSION['omeprazole_date_time'])){
    $omeprazole_date_time = DateTime::createFromFormat($format,$_SESSION['omeprazole_date_time']);
    $omeprazole_date_time = $omeprazole_date_time->format($format);
    $omeprazole_date_time = "'$omeprazole_date_time'";
}

if(isset ($_SESSION['omeprazole_no_of_tablets'])){
    $omeprazole_no_of_tablets = $_SESSION['omeprazole_no_of_tablets'];
}



//GAVISON
if(isset ($_SESSION['gavison_date_time'])){
    $gavison_date_time  = DateTime::createFromFormat($format, $_SESSION['gavison_date_time']);
    $gavison_date_time = $gavison_date_time->format($format);
    $gavison_date_time = "'$gavison_date_time'";
}

if(isset ($_SESSION['gavison_no_of_tablets'])){
    $gavison_no_of_tablets = $_SESSION['gavison_no_of_tablets'] ;
}



//ORAL MORPHINE
if(isset ($_SESSION['oralmorphine_date_time'])){
    $oralmorphine_date_time = DateTime::createFromFormat($format,$_SESSION['oralmorphine_date_time']);
    $oralmorphine_date_time = $oralmorphine_date_time->format($format);
    $oralmorphine_date_time = "'$oralmorphine_date_time'";
}

if(isset ($_SESSION['oralmorphine_no_of_tablets'])){
    $oralmorphine_no_of_tablets = $_SESSION['oralmorphine_no_of_tablets'] ;
}



//MORPHINE TABLETS
if(isset ($_SESSION['morphinetablets_date_time'])){
    $morphinetablets_date_time = DateTime::createFromFormat($format,$_SESSION['morphinetablets_date_time']);
    $morphinetablets_date_time = $morphinetablets_date_time->format($format);
    $morphinetablets_date_time = "'$morphinetablets_date_time'";
}

if(isset ($_POST['morphinetablets_no_of_tablets'])){
    $morphinetablets_no_of_tablets = $_SESSION['morphinetablets_no_of_tablets'];
}


//PARACETAMOL
if(isset ($_SESSION['paracetamol_date_time'])){
    $paracetamol_date_time = DateTime::createFromFormat($format,$_SESSION['paracetamol_date_time']);
    $paracetamol_date_time = $paracetamol_date_time->format($format);
    $paracetamol_date_time = "'$paracetamol_date_time'";
}

if(isset ($_SESSION['paracetamol_no_of_tablets'])){
    $paracetamol_no_of_tablets = $_SESSION['paracetamol_no_of_tablets'];
}



//CYCLIZINE
if(isset ($_SESSION['cyclizine_date_time'])){
    $cyclizine_date_time = DateTime::createFromFormat($format,$_SESSION['cyclizine_date_time']);
    $cyclizine_date_time = $cyclizine_date_time->format($format);
    $cyclizine_date_time = "'$cyclizine_date_time'";
}

if(isset ($_SESSION['cyclizine_no_of_tablets'])){
    $cyclizine_no_of_tablets = $_SESSION['cyclizine_no_of_tablets'];
}



//DEXAMETHAZONE
if(isset ($_SESSION['dexamethazone_date_time'])){
    $dexamethazone_date_time = DateTime::createFromFormat($format,$_SESSION['dexamethazone_date_time']);
    $dexamethazone_date_time = $dexamethazone_date_time->format($format);
    $dexamethazone_date_time = "'$dexamethazone_date_time'";
}

if(isset ($_SESSION['dexamethazone_no_of_tablets'])){
    $dexamethazone_no_of_tablets = $_SESSION['dexamethazone_no_of_tablets'];
}

/*
 * SESSION['SYMP'][LASKNDLAKS]
 * SESSION['HOSP'][KANSLCNASCL]
 * SESSION['MED'][ASKJDNASJ]
 */


$sql = <<<EOD
INSERT INTO medicationhistory(
  chemotherapy_date_time
, chemotherapy_no_of_tablets
, antisickness_date_time
, antisickness_no_of_tablets
, bowel_date_time
, bowel_no_of_tablets
, capecitabine_date_time
, capecitabine_no_of_tablets
, metoclopromide_date_time
, metoclopromide_no_of_tablets
, domperidone_date_time
, domperidone_no_of_tablets
, ondansetron_date_time
, ondansetron_no_of_tablets
, movicolsachet_date_time
, movicolsachet_no_of_tablets
, lansoprazole_date_time
, lansoprazole_no_of_tablets
, omeprazole_date_time
, omeprazole_no_of_tablets
, gavison_date_time
, gavison_no_of_tablets
, oralmorphine_date_time
, oralmorphine_no_of_tablets
, morphinetablets_date_time
, morphinetablets_no_of_tablets
, paracetamol_date_time
, paracetamol_no_of_tablets
, cyclizine_date_time
, cyclizine_no_of_tablets
, dexamethazone_date_time
, dexamethazone_no_of_tablets
, patID
    )
    VALUES (
      $chemotherapy_date_time
    , $chemotherapy_no_of_tablets
    , $antisickness_date_time
    , $antisickness_no_of_tablets
    , $bowel_date_time
    , $bowel_no_of_tablets
    , $capecitabine_date_time
    , $capecitabine_no_of_tablets
    , $metoclopromide_date_time
    , $metoclopromide_no_of_tablets
    , $domperidone_date_time
    , $domperidone_no_of_tablets
    , $ondansetron_date_time
    , $ondansetron_no_of_tablets
    , $movicolsachet_date_time
    , $movicolsachet_no_of_tablets
    , $lansoprazole_date_time
    , $lansoprazole_no_of_tablets
    , $omeprazole_date_time
    , $omeprazole_no_of_tablets
    , $gavison_date_time
    , $gavison_no_of_tablets
    , $oralmorphine_date_time
    , $oralmorphine_no_of_tablets
    , $morphinetablets_date_time
    , $morphinetablets_no_of_tablets
    , $paracetamol_date_time
    , $paracetamol_no_of_tablets
    , $cyclizine_date_time
    , $cyclizine_no_of_tablets
    , $dexamethazone_date_time
    , $dexamethazone_no_of_tablets

    , $patID
    );

EOD;

    $result = $conn->query($sql);

    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

    $conn->close();

    echo $chemotherapy_date_time."<br>";
    echo  $chemotherapy_no_of_tablets."<br>";
    echo  $antisickness_date_time."<br>";
    echo  $antisickness_no_of_tablets."<br>";
    echo  $bowel_date_time."<br>";
    echo  $bowel_no_of_tablets."<br>";
    echo  $capecitabine_date_time."<br>";
    echo  $capecitabine_no_of_tablets."<br>";
    echo  $metoclopromide_date_time."<br>";
    echo  $metoclopromide_no_of_tablets."<br>";
    echo  $domperidone_date_time."<br>";
    echo  $domperidone_no_of_tablets."<br>";
    echo  $ondansetron_date_time."<br>";
    echo  $ondansetron_no_of_tablets."<br>";
    echo  $movicolsachet_date_time."<br>";
    echo  $movicolsachet_no_of_tablets."<br>";
    echo  $lansoprazole_date_time."<br>";
    echo  $lansoprazole_no_of_tablets."<br>";
    echo  $omeprazole_date_time."<br>";
    echo  $omeprazole_no_of_tablets."<br>";
    echo  $gavison_date_time."<br>";
    echo  $gavison_no_of_tablets."<br>";
    echo  $oralmorphine_date_time."<br>";
    echo  $oralmorphine_no_of_tablets."<br>";
    echo  $morphinetablets_date_time."<br>";
    echo  $morphinetablets_no_of_tablets."<br>";
    echo  $paracetamol_date_time."<br>";
    echo  $paracetamol_no_of_tablets."<br>";
    echo  $cyclizine_date_time."<br>";
    echo  $cyclizine_no_of_tablets."<br>";
    echo  $dexamethazone_date_time."<br>";
    echo  $dexamethazone_no_of_tablets."<br>";
    ?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Navbar Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="dist/css/navbar.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container">

    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <a class="navbar-brand" href="medication_history.php">Back to medications</a>

        </div><!--/.container-fluid -->
    </nav>

    <ol class="breadcrumb">
        <li><a href="prototype/homescreen.html">Home</a></li>
        <li><a href="medication_history.php">Medications</a></li>
        <li class="active">Done</li>


    </ol>
    <div class="row">
        <div class="col-lg-12" align="center">
            <h1>Your medications have been recorded.</h1>
            <a href="prototype/homescreen.html"><button class="btn btn-lg btn-primary btn-block">Back To Home</button></a>
        </div>
    </div>







</div>

<!-- At the end -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="dist/js/bootstrap.min.js"></script>
<script src="dist/js/bootbox.min.js"></script>

</body>
</html>
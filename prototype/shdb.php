<?php
/**
 * Created by PhpStorm.
 * User: Shamail
 * Date: 1/30/2015
 * Time: 5:33 PM
 */

class shdb {


    protected $db_name = "sympdiary";
    protected $db_host = "localhost";
    protected $db_user = "root";
    protected $db_pass = "";

    protected $conn = null;

    public function connect()
    {
        //if(!is_null($this->conn)) return $this->conn;

        $this->conn = new mysqli($this->db_host,$this->db_user,$this->db_pass,$this->db_name);
        if($this->conn->connect_error)
        {
            die("Connection failed: ". $this->conn->connect_error);
        }
        else
        {
            //echo "Connected! "."<br><br>";
        }

        return $this->conn;
    }
    public function getConn()
    {
        return $this->conn;
    }
}
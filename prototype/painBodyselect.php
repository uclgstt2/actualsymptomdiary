<?php
/**
 * Created by PhpStorm.
 * User: Shamail
 * Date: 2/9/2015
 * Time: 6:13 PM
 */
session_start();



if(empty($_SESSION['symps']['pain']) || resetpain())
{
    $_SESSION['symps']['pain'] = [

        "Front left neck" => 0,
        "Front right neck" => 0,
        "Back left neck" => 0,
        "Back right neck" => 0,
        "Front left arm" => 0,
        "Front right arm" => 0,
        "Back left arm" => 0,
        "Back right arm" => 0,
        "Front left lung" => 0,
        "Front right lung" => 0,
        "Back left lung" => 0,
        "Back right lung"  => 0,
        "Front left tummy"  => 0,
        "Front right tummy"  => 0,
        "Back left lowerback"  => 0,
        "Back right lowerback"  => 0,
        "Front left leg"  => 0,
        "Front right leg"=> 0,
        "Back left leg" => 0,
        "Back right leg" => 0
    ];
}

function resetpain()
{
    if(!empty($_POST))
    {
        //print_r($_POST);
        if($_POST['reset']==1) return true;
        else return false;
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Navbar Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../dist/css/navbar.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container">

    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <a class="navbar-brand" href="allSymptoms.php">All Symptoms</a>
            <button class="navbar-btn btn btn-warning pull-right" onclick="resetpain()">Reset</button>
            <a href="allSymptoms.php"><button class="btn navbar-btn btn-success pull-right" style="margin-right: 20px">Done</button></a>
        </div><!--/.container-fluid -->
    </nav>

    <ol class="breadcrumb">
        <li><a href="homescreen.html">Home</a></li>
        <li><a href="allSymptoms.php">All Symptoms</a></li>
        <li class="active">Body Parts</li>

    </ol>
    <div class="row">
        <div class="col-lg-12" align="center">
            <h1>Tap on the colored circles to select body part</h1>
        </div>
    </div>

    <div class="row" >

        <div class="col-lg-12" id="body">
            <canvas id="painSelector"></canvas>
        </div>
    </div>





</div>

<!-- At the end -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="../dist/js/bootstrap.min.js"></script>
<script src="../dist/js/bootbox.min.js"></script>

<script>

    var severities = <?php echo json_encode($_SESSION['symps']['pain']); ?>;

    var canvas = document.getElementById("painSelector");

    var context = canvas.getContext("2d");


    var imgObj = new Image();
    imgObj.src = "../images/bodypoints.jpg";
    imgObj.onload = function()
    {
        var width = this.width;
        var height = this.height;
        canvas.width = width;
        canvas.height = height;
        context.drawImage(imgObj,5,5);
        setCircles();

    };


    function writeMessage(canvas, message) {
        var context = canvas.getContext('2d');
        document.getElementById("co").innerHTML=message;
    }
    function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
            x: evt.clientX - rect.left,
            y: evt.clientY - rect.top
        };
    }

    canvas.addEventListener('mousemove', function(evt) {
        var mousePos = getMousePos(canvas, evt);
        var message = 'Mouse position: ' + mousePos.x + ',' + mousePos.y;
        //writeMessage(canvas, message);
    }, false);

    canvas.addEventListener('click',function(evt){
        var mousePos = getMousePos(canvas, evt);
        //alert(mousePos.x+","+mousePos.y);
        //getDist()
        var bodypart = getPartInRange(30,mousePos.x,mousePos.y);
        var res = bodypart.replace(/[\ ]/g,"_");
        //alert(res);
        navigateTo(res);


    },false);

    var re = /(.+),(.+)/;
    function extractCoords(cordStr)
    {
        var points = re.exec(cordStr);
        //alert ("Mouse clicked coordinates(X : "+points[1]+" Y : "+points[2]+")");
        return [points[1],points[2]];

    }

    function getDist(x1,y1,x2,y2)
    {
        var x1x2 = x1-x2;
        var y1y2 = y1-y2;
        var dist = Math.sqrt(x1x2*x1x2 + y1y2*y1y2);
        return dist;
    }

    function getPartInRange(range,X,Y)
    {
        console.clear();
        var found = [];
        for(var i = 0;i < 20; i++)
        {
            var cordstr = bodypartCord[i];
            var points = extractCoords(cordstr);
            var dist = getDist(X,Y,points[0],points[1]);
            //console.log("IN : "+i+" DIST : "+dist+" PART : "+bodypartlist[i]);
            if(dist < range)
            {
                //console.log("--!-- FOUND IN : "+i+" DIST : "+dist+" PART : "+bodypartlist[i]);
                found.push(bodypartlist[i]);
            }
        }
        var len = found.length;
        for(var i = 0; i < len; i++)
        {
            console.log(found[i]);
        }
        return found[0];
    }

    function setCircles()
    {
        for(var i = 0; i < 20; i++)
        {
            var cordstr = bodypartCord[i];
            var bodypart = bodypartlist[i];

            var points = extractCoords(cordstr);
            var severity = severities[bodypart];
            //console.log("part: "+bodypart+" sev: "+severity);

            drawCircle(points[0],points[1],36,sevtoCol(severity));
        }
    }

    function sevtoCol(sev)
    {
        console.log("SevtoCol : "+sev);

        switch(sev)
        {
            case 0:
                return 'rgba(0,204,0,0.5)';
                break;
            case 1:
            case 2:
                return 'rgba(255,199,19,0.5)';
                break;
            case 3:
            case 4:
                return 'rgba(255,96,3,0.5)';
                break;
            case 5:
                return 'rgba(204,0,0,0.5)';
                break;
            default :
                return 'rgba(150,150,150,0.5)';
                break;
        }
    }

    function drawCircle(centerX,centerY,radius,color)
    {
        context.beginPath();
        context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
        context.fillStyle = color;
        context.fill();
        context.lineWidth = 2;
        context.strokeStyle = color;
        context.stroke();
    }
    var bodypartlist = [
        "Front left neck",
        "Front right neck",
        "Back left neck",
        "Back right neck",
        "Front left arm",
        "Front right arm",
        "Back left arm",
        "Back right arm",
        "Front left lung",
        "Front right lung",
        "Back left lung",
        "Back right lung",
        "Front left tummy",
        "Front right tummy",
        "Back left lowerback",
        "Back right lowerback",
        "Front left leg",
        "Front right leg",
        "Back left leg",
        "Back right leg"
    ];
    var bodypartCord = [
        "296,229",
        "387,229",
        "836.5,280",
        "922.5,276",
        "199.5,430",
        "472,436",
        "753.5,473",
        "1000.5,480",
        "277.5,335",
        "406.5,340",
        "826.5,420",
        "927.5,420",
        "305.5,525",
        "396.5,522",
        "832.5,516",
        "919.5,520",
        "286.5,790",
        "385,790",
        "821,838",
        "923,846"
    ];


    function navigateTo(bodypart)
    {
        window.location.href = "painSeverity.php?bodypart="+bodypart;
    }

    function resetpain()
    {
        var datastring = {"reset":1};
        var posting = $.post("painBodyselect.php",datastring, function()
        {
            setTimeout(function()
            {
                window.location.reload();
            },20);
        });
    }
</script>


</body>
</html>
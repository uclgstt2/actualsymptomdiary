<?php
/**
 * Created by PhpStorm.
 * User: Shamail
 * Date: 2/7/2015
 * Time: 5:20 PM
 */

include('shdb.php');

$db = new shdb();
$conn = $db->connect();

session_start();
//print_r($_SESSION);
$symptoms = $_SESSION['symps'];

$sympName = $_GET['symp'];

$sql = "SELECT * FROM symptoms,symptext WHERE symptoms.name = '$sympName' AND symptext.Symptom = symptoms.sympID";
$result = $conn->query($sql)->fetch_array(MYSQLI_ASSOC);

$sevToText = json_decode($result['sevTotext']);

$sevToAction = ($result['sevToAction']);

$sevToAction = str_replace("+","and",$sevToAction);

$sevToAction = json_decode($sevToAction);


if(!empty($_POST))
{
    print_r($_POST);

    $symp = $_POST['symp'];
    $sev = $_POST['sev'];

    $_SESSION['symps'][$symp] = intval($sev);

    echo "GOOD";
}
else
{
   // print_r($_SESSION);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Navbar Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../dist/css/navbar.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container">

    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="allSymptoms.php"> Back to Symptoms</a>

            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">

                </ul>
                <ul class="nav navbar-nav navbar-right">

                </ul>
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>

    <div class="row" style="margin-bottom: 12px;">
        <div class="col-xs-12">
            <div class="btn btn-block btn-lg btn-warning" style="background-color: #CC0000;"
                 onclick="saveToSession('<?php echo $sympName?>',5)"><?php $val = "3"; echo $sevToText->$val;?></div>
        </div>

    </div>
    <div class="row" style="margin-bottom: 12px">
        <div class="col-xs-12">
            <div class="btn btn-block btn-lg btn-danger" style="background-color: #ff6003;"
                 onclick="saveToSession('<?php echo $sympName?>',4)"><?php $val = "2"; echo $sevToText->$val;?></div>
        </div>

    </div>
    <div class="row" style="margin-bottom: 12px">
        <div class="col-xs-12">
            <div class="btn btn-block btn-lg btn-warning" style="background-color: #ffc713;"
                 onclick="saveToSession('<?php echo $sympName?>',3)"><?php $val = "1"; echo $sevToText->$val;?></div>
        </div>

    </div>
    <div class="row" style="margin-bottom: 12px">
        <div class="col-xs-12">
            <div class="btn btn-block btn-lg btn-success" style="background-color: #00CC00;"
                 onclick="saveToSession('<?php echo $sympName?>',0)"><?php $val = "0"; echo $sevToText->$val;?></div>
        </div>

    </div>


</div>

<!-- At the end -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="../dist/js/bootstrap.min.js"></script>
<script src="../dist/js/bootbox.min.js"></script>

<script>

    var sevToAction = <?php echo json_encode($sevToAction);?>;
    console.log(sevToAction);

    function saveToSession(symp, sev)
    {
        var datastring = {"symp":symp,"sev":sev};
        if( sev >= 2)
        {
            var message = sevToAction[sev-2];
            var mode = "info";
            if(sev == 3) mode = "warning";
            if(sev >= 4) mode = "danger";

            //alert('<div class="alert alert-'+ mode + '">' +message+"</div>");
            bootbox.dialog({
                title: "Advice",
                message: '<div class="alert alert-'+ mode + '">' +message+"</div>",
                buttons:{
                    success: {
                        label:"Got it!",
                        className: "btn-"+mode,
                        callback: function () {
                            var posting = $.get("saveToSession.php",datastring, function()
                            {
                                setTimeout(function()
                                {
                                    window.location.href = "allSymptoms.php";
                                },20);
                            });

                        }
                    }
                }
            });


        }
        else
        {
            var posting = $.get("saveToSession.php",datastring, function()
            {
                setTimeout(function()
                {
                    window.location.href = "allSymptoms.php";
                },20);
            });
        }
    }



</script>
</body>
</html>
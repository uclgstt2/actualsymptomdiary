<?php
/**
 * Created by PhpStorm.
 * User: Shamail
 * Date: 2/25/2015
 * Time: 7:20 PM
 */

include ('shdb.php');


$db = new shdb();
$conn = $db->connect();

$sql = "SELECT * FROM symplog";

$result = $conn->query($sql);

$logs = [];
while($row = $result->fetch_array(MYSQLI_ASSOC))
{
    $logs[$row['symplogID']] = $row;
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Navbar Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../dist/css/navbar.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container">

    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <a class="navbar-brand" href="homescreen.html">Edit Diary Log</a>

        </div><!--/.container-fluid -->
    </nav>

    <ol class="breadcrumb">
        <li><a href="homescreen.html">Home</a></li>
        <li class="active">Edit Symptoms</li>

    </ol>



    <table class="table" style="margin-top: 100px">
        <th>Log ID</th>
        <th>Date</th>
        <th>Edit</th>

        <?php
        $i = -1;
        foreach($logs as $ID => $row)
        {
            ?>

            <tr>
                <td><?php echo $ID?></td>
                <td><?php echo $row["Date"]?></td>
                <td>
                    <div class="btn-group" role="group" aria-label="...">

                        <a href='editmode.php?ID=<?php echo $ID; ?>'><button class="btn btn-xs btn-info">EDIT</button></a>
                    </div>
                </td>
            </tr>

        <?php
        }
        ?>

    </table>

</div>

<!-- At the end -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="../dist/js/bootstrap.min.js"></script>

</body>
</html>
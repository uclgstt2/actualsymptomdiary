<?php
/**
 * Created by PhpStorm.
 * User: Shamail
 * Date: 2/7/2015
 * Time: 5:20 PM
 */

include('shdb.php');

$db = new shdb();
$conn = $db->connect();

session_start();
//print_r($_SESSION);
$pain = $_SESSION['symps']['pain'];
//print_r($pain);
$bodypart = $_GET['bodypart'];



if(!empty($_POST))
{
    //print_r($_POST);

    $part = $_POST['part'];
    $sev = $_POST['sev'];

    $_SESSION['symps']['pain'][$part] = intval($sev);

    echo "GOOD";
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Navbar Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../dist/css/navbar.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container">

    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <a class="navbar-brand" href="painBodyselect.php">Body Parts</a>

        </div><!--/.container-fluid -->
    </nav>

    <ol class="breadcrumb">
        <li><a href="homescreen.html">Home</a></li>
        <li><a href="allSymptoms.php">All Symptoms</a></li>
        <li><a href="painBodyselect.php">Body Parts</a></li>
        <li class="active" id="curpage"></li>

    </ol>

    <div class="row" style="margin-bottom: 12px;">
        <div class="col-xs-6" align="center">
            <img src="../images/pain0.jpg" onclick="saveToSession(0)">
        </div>
        <div class="col-xs-6" align="center">
            <img src="../images/pain1.jpg" onclick="saveToSession(1)">
        </div>
    </div>

    <div class="row" style="margin-bottom: 12px;">
        <div class="col-xs-6" align="center">
            <img src="../images/pain2.jpg" onclick="saveToSession(2)">
        </div>
        <div class="col-xs-6" align="center">
            <img src="../images/pain3.jpg" onclick="saveToSession(3)">
        </div>
    </div>

    <div class="row" style="margin-bottom: 12px;">
        <div class="col-xs-6" align="center">
            <img src="../images/pain4.jpg" onclick="saveToSession(4)">
        </div>
        <div class="col-xs-6" align="center">
            <img src="../images/pain5.jpg" onclick="saveToSession(5)">
        </div>
    </div>


</div>

<!-- At the end -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="../dist/js/bootstrap.min.js"></script>
<script src="../dist/js/bootbox.min.js"></script>

<script>

    var bodypart = <?php echo json_encode($bodypart);?>;
    var res = bodypart.replace(/_/g," ");


    var li = document.getElementById('curpage');
    li.innerHTML = "<b>"+res+"<b>";

    function saveToSession(sev)
    {
        var mySev = sev+0;
        var datastring = {"part":res,"sev":mySev};


        var posting = $.post("painSeverity.php",datastring, function()
        {
            setTimeout(function()
            {
                window.location.href = "painBodyselect.php";
            },20);
        });

    }

    function underscoreTospace (string)
    {
        var res = string.replace(/_/g," ");
        return res;
    }



</script>
</body>
</html>
<?php


include ('shdb.php');
$db = new shdb();
$conn = $db->connect();

$sql = "SELECT * FROM symptoms";

$result = $conn->query($sql);

session_start();

//stores name as keys and the severity selected as value
/*
 * symptoms = [
 *      "Nausea" => -1,
 *      "Vomiting => -1,
 * ]
 */
$symptoms = [];
while($row = $result->fetch_array(MYSQLI_ASSOC))
{
    $symptoms[$row['Name']] = -1;
}

if(isset($_SESSION['symps']))
{
    $symptoms = $_SESSION['symps'];
}
else
{
    $_SESSION['symps'] = $symptoms;
    $_SESSION['symps']['pain'] = [

        "Front left neck" => 0,
        "Front right neck" => 0,
        "Back left neck" => 0,
        "Back right neck" => 0,
        "Front left arm" => 0,
        "Front right arm" => 0,
        "Back left arm" => 0,
        "Back right arm" => 0,
        "Front left lung" => 0,
        "Front right lung" => 0,
        "Back left lung" => 0,
        "Back right lung"  => 0,
        "Front left tummy"  => 0,
        "Front right tummy"  => 0,
        "Back left lowerback"  => 0,
        "Back right lowerback"  => 0,
        "Front left leg"  => 0,
        "Front right leg"=> 0,
        "Back left leg" => 0,
        "Back right leg" => 0
    ];
}

//print_r($symptoms);
$btn = '<button class="navbar-btn btn btn-success pull-right" onclick="savebtn()">Save!</button>';
foreach($symptoms as $key=>$value)
{
   // printf("$key : $value -----");
    if($value < 0)
    {
        $btn = '<button class="navbar-btn btn btn-info pull-right" disabled="disabled">Set all Symptoms first</button>';
        break;
    }
}

if(isset($_GET['end']))
    endsession();

function endsession()
{
    $_SESSION = array();
    session_destroy();
    session_unset();
    header("Location:allSymptoms.php");
    die();
}
?>







<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Navbar Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../dist/css/navbar.css" rel="stylesheet">

</head>

<body>

<div class="container">

    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <a class="navbar-brand" href="homescreen.html">Back to Home Page</a>
            <button class="navbar-btn btn btn-warning pull-right" style="margin-left: 20px" onclick="endbtn()">Reset</button>
            <?php echo $btn; ?>

        </div><!--/.container-fluid -->
    </nav>

    <ol class="breadcrumb">
        <li><a href="homescreen.html">Home</a></li>

        <li class="active">All Symptoms</li>

    </ol>
    <?php

    function printSymps ($symps)
    {
        //print_r($symps);
        $count = count($symps);

        $i = 0;
        //$count = 0;
        $keys = array_keys($symps);

        for($i = 0; $i < $count-1;)
        {
            $name = $keys[$i];
            if($name == "pain")
            {
                $i++;
                continue;
            }

           echo('<div class="row" style="margin-bottom: 20px; text-align: center" >');
            for($j = 0; $j < 2; $j++ )
            {
                $name = $keys[$i++];
                $severity = $symps[$name];
               echo getSympButton($name,$severity);
            }

            echo "</div>";
        }
    }

    function getSympButton($name,$severity)
    {
        $btnType = "default";


        switch ($severity)
        {
            case 0:
            case 1:
                $btnType = "success";
                break;
            case 2:
            case 3:
                $btnType = "warning";
                break;
            case 4:
            case 5:
            case 6:
                $btnType = "danger";
                break;

        }

        $str = "<div class='col-xs-6'>".
            "<div class='btn btn-lg btn-block btn-$btnType' id='$name' onclick='btnClick(\"$name\")'>$name</div>".
            "</div>";

        //echo $str;
        return $str;

    }


    function mode($print)
    {
        if(!isset($_SESSION['symps']['pain']))
            return -1;
        $array = $_SESSION['symps']['pain'];
        //$valueArray contains the values -> frequency
        // traverse the values, values become keys
        // everytime the same value is hit, increment
        $valueArray = [];
        foreach ($array as $key=>$value) {
            if(array_key_exists($value,$valueArray))
            {
                $valueArray[$value]++;
            }
            else
                $valueArray[$value] = 0;
        }

        $maxFrVal = max($valueArray);
        if($print == 1)
        {
            print_r($valueArray);
            print_r($maxFrVal);
        }
        $mode = 0;
        foreach ($valueArray as $key=>$value) {
            if($value == $maxFrVal) $mode = $key;
        }

        return $mode;
    }

    function sevToCol($sev)
    {
        //print_r($sev);
        $col = "default";
        switch($sev)
        {
            case 0:
            case 1:
                $col = "success";
                break;
            case 2:
            case 3:
                $col = "warning";
                break;
            case 4:
            case 5:
                $col = "danger";
                break;
            default:
                break;

        }
        return $col;
    }

    ?>

    <?php printSymps($symptoms);?>

    <div class="row">
        <div class="col-lg-12">

            <button class="btn btn-<?php echo sevToCol(mode(0)); ?> btn-block btn-lg" onclick="window.location.href='painBodyselect.php'">Pain body select</button>
            <?php //echo mode(1); ?>

        </div>
    </div>

</div>



<!-- At the end -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="../dist/js/bootstrap.min.js"></script>
<script>


    function btnClick(sympName)
    {
        //alert(sympName);
       if(sympName == "Pain")
       {
           window.location.href = "painLocation.php";
       }
        else
       {
           window.location.href = "showSeverity.php?symp="+sympName;
       }

    }

    function endbtn()
    {
        window.location.href="allSymptoms.php?end=true";
    }


    function savebtn()
    {
        window.location.href = "saveSympData.php";
    }

</script>


</body>

</html>
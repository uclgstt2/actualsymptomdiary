<?php
/**
 * Created by PhpStorm.
 * User: Shamail
 * Date: 2/25/2015
 * Time: 7:37 PM
 * used to set the ID in session, in order to update not insert
 * load the data from database
 * convert it to assoc arrays
 * save it into session
 * navigate to allSymptoms
 *
 * saving needs changing. Allow for update if $_SESSION['symp']['id] exists.
 */

include 'shdb.php';
session_start();

$db = new shdb();
$conn = $db->connect();

$id = $_GET['ID'];

$sql = "SELECT SympJSON FROM symplog WHERE symplogID = $id";

$sympJSON = $conn->query($sql)->fetch_array(MYSQLI_NUM)[0];

print_r($sympJSON);

echo '<br><br>';

$decoded = json_decode($sympJSON,true);

print_r($decoded);

$_SESSION['symps'] = $decoded;
$_SESSION['symps']['ID'] = $id;

header('Location:allSymptoms.php');
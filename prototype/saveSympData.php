<?php
/**
 * Created by PhpStorm.
 * User: Shamail
 * Date: 2/25/2015
 * Time: 6:04 PM
 */

include ('shdb.php');
session_start();


$db = new shdb();
$conn = $db->connect();

$sympData = $_SESSION['symps'];

$patID = 1;
if(!empty($_SESSION['user']['id']))
{
    $patID = $_SESSION['user']['id'];
}

if(isset($_SESSION['symps']['ID']))
{
    $id = $_SESSION['symps']['ID'];
    unset($_SESSION['symps']);

    $jsonSymp = json_encode($sympData);
    $sql = "UPDATE symplog SET SympJSON = '$jsonSymp' WHERE symplogID = $id";
}
else
{
    $jsonSymp = json_encode($sympData);
    $sql = "INSERT INTO symplog (`symplogID`,`patientID`,`Date`,`SympJSON`)
        VALUES (NULL,$patID,CURRENT_TIMESTAMP,'$jsonSymp');";
}

$conn->query($sql);

if($conn->error)
    print_r("ERROR : $conn->error");
else
    print_r("save success");

function endsession()
{
    $_SESSION = array();
    session_destroy();
    session_unset();
    //header("Location:allSymptoms.php");
    //die();
}

endsession();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Navbar Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../dist/css/navbar.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container">

    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <a class="navbar-brand" href="allSymptoms.php">All Symptoms</a>

        </div><!--/.container-fluid -->
    </nav>

    <ol class="breadcrumb">
        <li><a href="homescreen.html">Home</a></li>
        <li><a href="allSymptoms.php">All Symptoms</a></li>
        <li class="active">Done</li>


    </ol>
    <div class="row">
        <div class="col-lg-12" align="center">
            <h1>Your symptoms have been recorded</h1>
            <a href="homescreen.html"><button class="btn btn-lg btn-primary btn-block">Back To Home</button></a>
        </div>
    </div>







</div>

<!-- At the end -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="../dist/js/bootstrap.min.js"></script>
<script src="../dist/js/bootbox.min.js"></script>

</body>
</html>
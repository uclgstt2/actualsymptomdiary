<?php
session_start ();

if (isset ( $_POST['hour'] )) {
    echo ($_POST['hour'] . '<br>');
}

?>


    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">
        <link rel="stylesheet" type="text/css" href="./jquery.datetimepicker.css"/>

        <title>Navbar Template for Bootstrap</title>

        <!-- Bootstrap core CSS -->
        <link href="dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="dist/css/navbar.css" rel="stylesheet">
        <link href="dist/css/jquery.datetimepicker.css" rel="stylesheet" TYPE="TEXT/CSS">


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body style="background: #4d7aa9">

    <div class="container">

        <!-- Static navbar -->
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Symptom Diary</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">

                    </ul>
                    <ul class="nav navbar-nav navbar-right">

                    </ul>
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>


        <article>
            <header>
                <a href="homescreen.html" class="homesymptom">< Home</a>
                <h1>
                    <center>Medication History</center>
                </h1>
            </header>

            <center>
                <p>When did you take your lansoprazole tablets?</p>



                <form action="medication_history.php" method="post">

                    <input type="text" name="lansoprazole_time_date" id="datetimepicker"/><br><br>



                    <p>How many did you take?</p>


                    <select name="lansoprazole_no_of_tablets">
                        <?php
                        for($num = 0; $num < 6; $num ++) {
                            echo ("<option value=" . $num . ">$num</option >");
                        }
                        ?>

                    </select> <br></br>

                    <!-- 		<a href="medication_history.php" class="homepagebegin">Submit</a> -->



                    <button class="btn btn-default" type="submit">Submit</button>
                </form>

            </center>
            <footer>

                <p>


                <center>From Guy's and St Thomas' Hospital</center>

                <div style="float: right;">

                    <a href="apphelp.html" class="btn">App Info</a>
                </div>

                </p>

            </footer>
    </div>


    </body>

    <script src="./jquery.js"></script>

    <script src="./jquery.datetimepicker.js"></script>


    <script>

        $('#datetimepicker').datetimepicker({
            dayOfWeekStart : 1,
            lang:'en',
            disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
            startDate:	'2015/01/05'
        });
        $('#datetimepicker').datetimepicker({value:'2015/04/15 05:03',step:10});




    </script>
    </html>

<?php
unset ( $_POST );
?>
<?php
/**
 * Created by PhpStorm.
 * User: Shamail
 * Date: 3/4/2015
 * Time: 10:03 AM
 */
session_start();

$medlog = [];
/*
 * $medlog['medname'] = [amount,time];
 */
if(isset($_SESSION['medlog']))
{
   $medlog = $_SESSION['medlog'];
}
else
{
    $_SESSION['medlog'] = $medlog;
}


//=============================================================================================

include("../db_conn.php");

$db = new DBconn();
$conn = $db->connect();

$medlist = [];
if(!isset($_SESSION['medlist']))
{
    $sql = "SELECT * FROM medicines";
    $result = $conn->query($sql);

    while($row = $result->fetch_array(MYSQLI_NUM))
    {
        // by default, no medicine taken
        $medlist[$row[0]] = trim($row[1]);
    }

    $_SESSION['medlist'] = $medlist;

}
else
{
    $medlist = $_SESSION['medlist'];
}

$date = date("Y-m-d");
if(isset($_SESSION['medlog']['date']))
{
    $date = $_SESSION['medlog']['date'];
}
else{
    $_SESSION['medlog']['date'] = $date;
}


//=============================================================================================


function printButtons($medlist)
{
    $medlog = $GLOBALS['medlog'];
    for($i = 1; $i < count($medlist);)
    {
        if($medlist[$i] == "date") continue;


        echo('<div class="row" style="margin-bottom: 10px">');
        for($j = 0; $j < 2; $j++ )
        {
            $name = $medlist[$i++];
            //$name = trim($name," ");

            $btntype = "default";
            if(array_key_exists($name,$medlog)) $btntype = "primary";

            echo "<div class='col-lg-6'>".
                "<a href='MedLogEntry.php?med=$name'><button class='btn btn-lg btn-$btntype btn-block'>$name</button></a>".
                "</div>";
        }

        echo "</div>";
    }
}


$GLOBALS['medlog'] = $medlog;


?>

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <link rel="stylesheet" type="text/css" href="../jquery.datetimepicker.css"/>

    <title>Navbar Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../dist/css/navbar.css" rel="stylesheet">
    <link href="../dist/css/jquery.datetimepicker.css" rel="stylesheet" TYPE="TEXT/CSS">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="container">

    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <a class="navbar-brand" href="prototype/homescreen.html">Back to Home Page</a>
            <!--<button class="navbar-btn btn btn-success pull-right" style="margin-left: 20px" onclick="done()">Done</button>-->
        </div><!--/.container-fluid -->
    </nav>

    <ol class="breadcrumb">
        <li><a href="../prototype/homescreen.html">Home</a></li>

        <li class="active">Medication</li>

    </ol>



    <article>
        <header>

            <h1><center>Medication History</center></h1>
            <h2><center>What medication are you taking?</center></h2>

        </header>
        <div class="row">
            <div class="col-lg-3 pull-right">
                <label>Select the date when you took these medicines</label>
                <div class="form-group">
                    <input type='text'  id='dtpkr' class="form-control" value="05-2-15"/>
                </div>
            </div>
        </div>

        <center>
            <?php printButtons($medlist); ?>

            <br>
            <br>
            <a style="float" href="saveMedData.php" class="homesymptom"><button class="btn btn-lg btn-primary btn-block">Submit</button></a>

        </center>


        <footer>

            <p>
            <center>
                From Guy's and St Thomas' Hospital
            </center>

            <div style="float: right;">

                <a href="apphelp.html" class="btn"><button class="btn btn-lg btn-warning">App Info</button></a>
            </div>
            </p>

        </footer>

    <!-- At the end -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="../dist/js/bootstrap.min.js"></script>
    <script src="../jquery.datetimepicker.js"></script>

        <script>
        var elem = document.getElementById("dtpkr");
        elem.value = "<?php print_r($date) ?>";

            $('#dtpkr').datetimepicker({
                timepicker :false,
                format:'Y-m-d',
                onChangeDateTime:function(dp,$input){

                    var date = $input.val();
                    var datastring = {"date":date};
                    var getting = $.get("saveDate.php",datastring,function(data){
                        //alert(data);
                    });
                }
            });
        </script>
</body>
</html>

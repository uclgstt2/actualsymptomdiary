<?php
/**
 * Created by PhpStorm.
 * User: Shamail
 * Date: 3/4/2015
 * Time: 10:04 AM
 */

session_start();
$med = $_POST['name'];

$time = $_POST['time'];
$amount = $_POST['amount'];
$id = 1;
foreach($_SESSION['medlist'] as $key=>$value)
{
    if($value == $med){
        $id = $key;
        break;
    }
}

$_SESSION['medlog'][$med] = [
    "id" => $id,
    "time" => $time,
    "amount" => $amount
];

echo json_encode($_SESSION['medlog']);
<?php
/**
 * Created by PhpStorm.
 * User: Shamail
 * Date: 3/4/2015
 * Time: 10:04 AM
 */

$med = $_GET['med'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <link rel="stylesheet" type="text/css" href="../jquery.datetimepicker.css"/>

    <title>Navbar Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../dist/css/navbar.css" rel="stylesheet">
    <link href="../dist/css/jquery.datetimepicker.css" rel="stylesheet" TYPE="TEXT/CSS">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>


<body>
<div class="container">

    <!-- Static navbar -->
    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <a class="navbar-brand" href="../prototype/homescreen.html">Medicine Log</a>

        </div><!--/.container-fluid -->
    </nav>

    <ol class="breadcrumb">
        <li><a href="../prototype/homescreen.html">Home</a></li>
        <li><a href="allMedicines.php">Medications</a></li>
        <li class="active"><?php echo $med?></li>

    </ol>

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Medicine History</h1>
            </div>
            <div class="col-lg-12">
                <form>
                    <?php
                    switch($med){
                        case "movicol":
                            $med_name="$med sachets";
                            break;

                        case "morphine_tablets":
                            $med_name="morphine tablets";
                            break;

                        default: $med_name = "$med tablets";
                            break;
                    }
                    ?>
                    <label>When did you take your <?php echo $med_name?></label>
                    <div class="form-group">
                        <input type='text'  id='dtpkr' class="form-control"/>
                    </div>

                    <label>How much did you take?</label>
                    <div class="form-group">
                        <input type='number'  id='amount' class="form-control"/>
                    </div>
                </form>

            </div>
            <div class="col-lg-4">
                <button class="btn btn-success btn-lg btn-block" onclick="saveMedLog()">Save</button>
            </div>

        </div>
    </div>



        <footer>



            <center>From Guy's and St Thomas' Hospital</center>

            <div style="float: right;">

                <a href="../apphelp.html" class="btn"><button class="btn btn-lg btn-warning">App Info</button></a>
            </div>



        </footer>
</div>


</body>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="../dist/js/bootstrap.min.js"></script>
<!--http://xdsoft.net/jqplugins/datetimepicker/-->
<script src="../jquery.datetimepicker.js"></script>


<script>


    $("#dtpkr").datetimepicker({
            datepicker:false,
            format:'H:i'
    });
    var medicine = "<?php echo $med ?>";
    function getData()
    {
       var time = $("#dtpkr").val();
        var amount = $("#amount").val();


        console.log( "time : "+ time + " amount : "+amount);

        return {"name" : medicine, "time" : time, "amount" : amount};

    }

    function saveMedLog()
    {
        var datastring = getData();
        var posting = $.post("saveMedToSession.php",datastring,function(data)
        {
            console.log(data);
            setTimeout(function() {
                window.location.href =  "allMedicines.php";
            },500);
        });
    }





</script>
</html>

<?php
$servername = "localhost";
$username = "root";
$password = "golfgolf";
$dbname = "symptomdiary";

// Create connection
$conn = new mysqli($servername, $username, $password);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
echo "Connected successfully";

// sql to create table
$sql = "CREATE TABLE MyGuests (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
patientname VARCHAR(30) NOT NULL,
patienthousenumber VARCHAR(30) NOT NULL,
patientpin VARCHAR(50),
)";

if ($conn->query($sql) === TRUE) {
    echo "Table MyGuests created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}
?>
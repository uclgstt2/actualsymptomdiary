-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
<<<<<<< HEAD
-- Generation Time: Feb 04, 2015 at 08:02 PM
=======
-- Generation Time: Feb 04, 2015 at 07:57 PM
>>>>>>> bpLogin
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sympdiary`
--

-- --------------------------------------------------------

--
-- Table structure for table `diarylogs`
--

CREATE TABLE IF NOT EXISTS `diarylogs` (
  `logID` int(11) NOT NULL AUTO_INCREMENT,
  `patientID` int(11) NOT NULL,
  `date` date NOT NULL,
  `Diarrhoea` int(11) NOT NULL,
  `Constipation` int(11) NOT NULL,
  `Nausea` int(11) NOT NULL,
  `Vomiting` int(11) NOT NULL,
  `Swallowing` int(11) NOT NULL,
  `Distention` int(11) NOT NULL,
  `Energy` int(11) NOT NULL,
  `Appetite` int(11) NOT NULL,
  `riskRating` float NOT NULL,
  PRIMARY KEY (`logID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `diarylogs`
--

INSERT INTO `diarylogs` (`logID`, `patientID`, `date`, `Diarrhoea`, `Constipation`, `Nausea`, `Vomiting`, `Swallowing`, `Distention`, `Energy`, `Appetite`, `riskRating`) VALUES
(1, 1, '2014-12-01', 2, 4, 0, 1, 0, 4, 2, 1, 3.25),
(2, 4, '2014-12-01', 2, 4, 0, 1, 0, 4, 2, 1, 3.25),
(3, 1, '2014-12-01', 2, 4, 0, 1, 0, 4, 2, 1, 3.25),
(4, 1, '2014-12-01', 2, 4, 0, 1, 0, 4, 2, 1, 3.25),
(5, 6, '2014-12-02', 5, 0, 4, 0, 5, 0, 4, 1, 2.625),
(6, 3, '2014-12-04', 1, 3, 1, 5, 3, 3, 3, 3, 2.25),
(7, 1, '2014-12-04', 2, 1, 1, 3, 3, 3, 3, 3, 2.625),
(8, 1, '2014-12-08', 1, 5, 0, 5, 3, 3, 3, 1, 2.375),
(9, 3, '2014-12-08', 1, 5, 0, 5, 3, 3, 3, 1, 2.375),
(10, 3, '2014-12-10', 1, 4, 3, 3, 3, 3, 3, 3, 2.125),
(11, 9, '2014-12-10', 1, 5, 5, 2, 4, 1, 3, 3, 2),
(12, 9, '2014-12-11', 1, 5, 5, 2, 4, 1, 3, 3, 2),
(13, 1, '2014-12-11', 4, 1, 5, 2, 4, 1, 3, 3, 2.125),
(14, 1, '0000-00-00', 3, 3, 3, 3, 3, 3, 3, 3, 2),
(15, 1, '0000-00-00', 3, 3, 3, 3, 3, 3, 3, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `hosphist`
--

CREATE TABLE IF NOT EXISTS `hosphist` (
  `hosphistID` int(11) NOT NULL AUTO_INCREMENT,
  `patientID` int(11) NOT NULL,
  `Details` text NOT NULL,
  `Date` date NOT NULL,
  `EMRev` text,
  `GPRev` text,
  `outcome` text,
  PRIMARY KEY (`hosphistID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `hosphist`
--

INSERT INTO `hosphist` (`hosphistID`, `patientID`, `Details`, `Date`, `EMRev`, `GPRev`, `outcome`) VALUES
(1, 1, 'I called the hospital today to talk about the little pain i had somewhere', '2014-12-03', NULL, NULL, NULL),
(2, 1, 'I called the hospital today just to know what i should do as i missed my medicine', '2014-12-01', NULL, NULL, NULL),
(3, 2, 'the call was short and i am happy', '2014-12-04', NULL, NULL, NULL),
(4, 5, 'I was having difficulty swallowing so i did call and it was okay', '2014-12-04', 'I was given a review', NULL, NULL),
(5, 5, 'The hospital helps a lot. i really like it', '2014-12-02', NULL, NULL, NULL),
(6, 4, 'today i was feeling a bit drowsy so i called the hospital cand the receptionist picke up the phone to whom i said hi im not feeling so well and then he put me in touch with my doctor', '2014-12-06', 'they said i should get a check up. They thought i was overdosing', NULL, NULL),
(7, 4, 'today i called not my hospital but my gp . the gp was not there so i had to call him later as well', '2014-12-08', NULL, NULL, NULL),
(8, 3, 'my first calling record', '2014-12-01', NULL, NULL, NULL),
(9, 3, 'my second calling log. im getting the hang of it. i wish i could say the same for my medicine schedule', '2014-12-02', NULL, NULL, NULL),
(10, 3, 'today was a fairly sickinnig day i felt like vomitting the whole day so i did call the hospital and they asked me to take a few steps which i took and now i feel better', '2014-12-03', NULL, 'I went to my gp as suggested by the app as i was quite sick today', 'They said next week follow up'),
(11, 3, 'today i called the hospital twice as they didnt pick the phone up at the first time. i told them i was having big pain in my tummy. they told me to come visit the hospital', '2014-12-04', 'I was given a emergency review as i was quite unwell', NULL, 'They said come the day after tomorrow');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` text NOT NULL,
  `patientID` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `medicines`
--

CREATE TABLE IF NOT EXISTS `medicines` (
  `medID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text NOT NULL,
  PRIMARY KEY (`medID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `medicines`
--

INSERT INTO `medicines` (`medID`, `Name`) VALUES
(1, 'Metoclopromide'),
(2, 'Domperidone '),
(3, 'Ondansetron '),
(4, 'Capecitabine '),
(5, 'Movicol sachet '),
(6, 'Lansoprazole '),
(7, 'Omeprazole '),
(8, 'Gavison '),
(9, 'Oral morphine'),
(10, 'Morphine slow release tablets '),
(11, 'Paracetamol '),
(12, 'Cyclizine '),
(13, 'Dexamethasone ');

-- --------------------------------------------------------

--
-- Table structure for table `patientmeds`
--

CREATE TABLE IF NOT EXISTS `patientmeds` (
  `patientID` int(11) NOT NULL,
  `medID` int(11) NOT NULL,
  `Dose` text NOT NULL,
  `Frequency` text NOT NULL,
  `Type` text NOT NULL,
  PRIMARY KEY (`patientID`,`medID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This stores the dossage box patients set up in the app';

--
-- Dumping data for table `patientmeds`
--

INSERT INTO `patientmeds` (`patientID`, `medID`, `Dose`, `Frequency`, `Type`) VALUES
(1, 1, '20mg', '3 times a day. 5 days a week', 'Core'),
(1, 2, '400mg', '1 times a day. Every day.', 'Supplementry'),
(2, 2, '10mg', '2 times a day. Every day.', 'Core'),
(2, 4, '30mg', '2 times a day. 3 days a week', 'Core');

-- --------------------------------------------------------

--
-- Table structure for table `patientmedslog`
--

CREATE TABLE IF NOT EXISTS `patientmedslog` (
  `medslogID` int(11) NOT NULL,
  `patientID` int(11) NOT NULL,
  `Details` text NOT NULL,
  `CoreTaken` int(11) NOT NULL,
  `ExtraTaken` int(11) NOT NULL,
  `Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE IF NOT EXISTS `patients` (
  `patID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text NOT NULL,
  `Age` int(11) NOT NULL,
  `Address` text NOT NULL,
  `Pin` int(11) NOT NULL,
  PRIMARY KEY (`patID`),
  KEY `patID` (`patID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`patID`, `Name`, `Age`, `Address`, `Pin`) VALUES
(1, 'John Doe', 20, '11 Drummond Street', 0),
(2, 'Sean Paul', 40, '12 Drummond Street', 0),
(3, 'Louis Lane', 36, '10 Drummond Street', 0),
(4, 'Larry Burton', 50, '14 Drummond Street', 0),
(5, 'Peter Steve', 48, '16 Drummond Street', 0),
(6, 'Karp Nueman', 61, '61 Drummond Street', 0),
(7, 'Kanye East', 61, '17 Drummond Street', 0),
(8, 'Lasnium Holle', 58, '16 Drummond Street', 0),
(9, 'Lorde Patinson', 32, '18 Drummond Street', 0),
(10, 'Lee Uion', 42, '9 Drummond Street', 0),
(11, 'Oller Twist', 16, '7 Drummond Street', 0),
(12, 'Martin Ricky', 66, '66 Drummond Street', 0),
(13, 'Sky Latinson', 23, '23 Drummond Street', 0),
(14, 'Holland Roden', 27, '24 Drummond Street', 0),
(15, 'Bruce Baker', 33, '6 Drummond Street', 0),
(16, 'Drake Swift', 21, '21 Drummond Street', 0),
(17, 'Veronica Isabelle', 55, '55 Drummond Street', 0),
(18, 'John Holmes', 50, '21 Drummond Street', 0);

-- --------------------------------------------------------

--
-- Table structure for table `symptext`
--

CREATE TABLE IF NOT EXISTS `symptext` (
  `symtxtID` int(11) NOT NULL AUTO_INCREMENT,
  `Symptom` int(11) NOT NULL,
  `sevTotext` text NOT NULL,
  `sevToAction` text NOT NULL,
  PRIMARY KEY (`symtxtID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `symptext`
--

INSERT INTO `symptext` (`symtxtID`, `Symptom`, `sevTotext`, `sevToAction`) VALUES
(1, 1, '{"0":"none", "1":"2-3 bowel motions","2":"4-6 bowel motions", "3":"7+ bowel movements"}', '{"0":"none",\n\n"1":"Loperamide after each loose bowel motion for 24 hours + Increase fluids + Keep monitoring output",\n\n"2":"Loperamide after each loose bowel motion for 24 hours + Increase fluids + If not improvement stop drugs which might be causing diarrhoea and call Acute Oncology Service",\n\n"3":"Call Acute Oncology Service"}\n'),
(2, 2, '{"0":"none",\r\n\r\n"1":"No bowel movement within 24 hours",\r\n\r\n"2":"No bowel movement in 48 hours",\r\n\r\n"3":"No bowel movement in  72 hours"}', '{"0":"none",\r\n\r\n"1":"Increased fluids + Stop constipating drugs (list) + Try laxatives or speak to GP for advise",\r\n\r\n"2":"Increased fluids + Stop constipating drugs (list) + Try laxatives or speak to GP for advise",\r\n\r\n"3":"Call Acute Oncology"}\r\n'),
(3, 3, '{"0":"none",\r\n\r\n"1":"Feeling nauseous but not affecting oral intake",\r\n\r\n"2":"Feeling nauseous and oral intake decreased but not significant weight loss, dehydration",\r\n\r\n"3":"Feeling nauseous and not eating and drinking enough"}', '{"0":"none",\r\n\r\n"1":"Trial anti sickness + Keep up fluids + Eat small",\r\n\r\n"2":"Trial anti sickness + Keep up fluids + Eat small",\r\n\r\n"3":"Call Acute Oncology"}'),
(4, 4, '{"0":"none",\r\n\r\n"1":"1-2 episodes (separated by 5 minutes) in 24 hours",\r\n\r\n"2":"3-5 episodes (separated by 5 minutes) in 24 hours",\r\n\r\n"3":"More than 6 episodes in 24 hours"}', '{"0":"none",\r\n\r\n"1":"Trial anti sickness + Keep up fluids + Eat small meals",\r\n\r\n"2":"Trial anti sickness + Ask GP to review + additional anti-sickness + Keep up fluids + Eat small meals + Call Team if persist",\r\n\r\n"3":"Call Acute Oncology"}\r\n'),
(5, 5, '{"0":"none",\r\n\r\n"1":"Able to swallow some solids, some getting stuck",\r\n\r\n"2":"Only able to swallow semi-solid food",\r\n\r\n"3":"Only able to manage liquids"}', '{"0":"none",\r\n\r\n"1":"Call team to arrange medical review + Keep fluids up",\r\n\r\n"2":"OCall team for urgent medical review + Keep fluids up",\r\n\r\n"3":"Call Acute Oncology Service"}'),
(6, 6, '{"0":"none",\r\n\r\n"1":"Abdomen bloated but not causing problems",\r\n\r\n"2":"Abdomen bloated and causing symptoms",\r\n\r\n"3":"Abdomen bloated and causing severe symptoms"}', '{"0":"none",\r\n\r\n"1":"Call Team to discuss clinical review",\r\n\r\n"2":"Call Team for urgent medical review",\r\n\r\n"3":"Call Acute Oncology"}'),
(7, 7, '{"0":"normal",\r\n\r\n"1":"Increased fatigue but not affecting normal activities",\r\n\r\n"2":"Moderate or causing difficulty performing some activities",\r\n\r\n"3":"Severe or loss of ability to perform some activities"}', '{"0":"",\r\n\r\n"1":"Rest + Ensure drinking plenty",\r\n\r\n"2":"Rest + Ensure drinking plenty + Contact team if persist",\r\n\r\n"3":"Call acute oncology team"}'),
(8, 8, '{"0":"good",\r\n\r\n"1":"Loss of appetite",\r\n\r\n"2":"Oral intake decreased but not + significant weight loss, dehydration",\r\n\r\n"3":"Not eating and drinking enough and losing weight, becoming dehydrated"}', '{"0":"",\r\n\r\n"1":"Eat small meals + Keep up fluids",\r\n\r\n"2":"Eat small meals + Keep up fluids",\r\n\r\n"3":"Call Acute Oncology Team"}');

-- --------------------------------------------------------

--
-- Table structure for table `symptoms`
--

CREATE TABLE IF NOT EXISTS `symptoms` (
  `sympID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text NOT NULL,
  PRIMARY KEY (`sympID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Store the symptoms recorded in the symptom diary' AUTO_INCREMENT=9 ;

--
-- Dumping data for table `symptoms`
--

INSERT INTO `symptoms` (`sympID`, `Name`) VALUES
(1, 'Diarrhoea'),
(2, 'Constipation'),
(3, 'Nausea'),
(4, 'Vomiting'),
(5, 'Swallowing'),
(6, 'Distention'),
(7, 'Energy'),
(8, 'Appetite');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
/**
 * Created by PhpStorm.
 * Date: 11/26/2014
 * Time: 11:20 AM
 */

class DBconn {

/*protected $db_name = "symptomdiary";
protected $db_host = "localhost";
protected $db_user = "root";
protected $db_pass = "golfgolf";
//protected $db_pass = "";
*/

    protected $db_name = "sympdiary";
    protected $db_host = "localhost";
    protected $db_user = "root";
    protected $db_pass = "";

 protected $conn = null;

    public function connect()
    {
        //if(!is_null($this->conn)) return $this->conn;

        $this->conn = new mysqli($this->db_host,$this->db_user,$this->db_pass,$this->db_name);
        if($this->conn->connect_error)
        {
            die("Connection failed: ". $this->conn->connect_error);
        }
        else
        {
            //echo "Connected! "."<br><br>";
        }

        return $this->conn;
    }
    public function getConn()
    {
        return $this->conn;
    }

    public function getScenarios()
    {
        $sql = "SELECT * FROM `scenarios`;";
        $result = $this->conn->query($sql);
        return $result->fetch_array(MYSQLI_NUM);

    }

}

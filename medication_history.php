<?php
session_start ();
$_SESSION ["patID"] = 1; //patID stored in SESSION, eg. pat 1 has logged in



//CHEMOTHERAPY
if(isset ($_POST['chemotherapy_date_time'])){
  $_SESSION['chemotherapy_date_time'] = $_POST['chemotherapy_date_time'];
}

if(isset ($_POST['chemotherapy_no_of_tablets'])){
  $_SESSION['chemotherapy_no_of_tablets'] = $_POST['chemotherapy_no_of_tablets'];
}

/*
if(isset ($_SESSION['chemotherapy_date_time'])) {
  echo($_SESSION['chemotherapy_date_time'] . '<br>');
}

if(isset ($_SESSION['chemotherapy_no_of_tablets'])){
  echo ($_SESSION['chemotherapy_no_of_tablets'] . '<br>');
}
*/

//ANTISICKNESS
if(isset ($_POST['antisickness_date_time'])){
  $_SESSION['antisickness_date_time'] = $_POST['antisickness_date_time'];
}

if(isset ($_POST['antisickness_no_of_tablets'])){
  $_SESSION['antisickness_no_of_tablets'] = $_POST['antisickness_no_of_tablets'];
}


//BOWEL
if(isset ($_POST['bowel_date_time'])){
  $_SESSION['bowel_date_time'] = $_POST['bowel_date_time'];
}

if(isset ($_POST['bowel_no_of_tablets'])){
  $_SESSION['bowel_no_of_tablets'] = $_POST['bowel_no_of_tablets'];
}



//CAPECITABINE
if(isset ($_POST['capecitabine_date_time'])){
  $_SESSION['capecitabine_date_time'] = $_POST['capecitabine_date_time'];
}

if(isset ($_POST['capecitabine_no_of_tablets'])){
  $_SESSION['capecitabine_no_of_tablets'] = $_POST['capecitabine_no_of_tablets'];
}


//METOCLOPROMIDE
if(isset ($_POST['metoclopromide_date_time'])){
  $_SESSION['metoclopromide_date_time'] = $_POST['metoclopromide_date_time'];
}

if(isset ($_POST['metoclopromide_no_of_tablets'])){
  $_SESSION['metoclopromide_no_of_tablets'] = $_POST['metoclopromide_no_of_tablets'];
}


//DOMPERIDONE
if(isset ($_POST['domperidone_date_time'])){
  $_SESSION['domperidone_date_time'] = $_POST['domperidone_date_time'];
}

if(isset ($_POST['domperidone_no_of_tablets'])){
  $_SESSION['domperidone_no_of_tablets'] = $_POST['domperidone_no_of_tablets'];
}


//ONDANSETRON
if(isset ($_POST['ondansetron_date_time'])){
  $_SESSION['ondansetron_date_time'] = $_POST['ondansetron_date_time'];
}

if(isset ($_POST['ondansetron_no_of_tablets'])){
  $_SESSION['ondansetron_no_of_tablets'] = $_POST['ondansetron_no_of_tablets'];
}


//MOVICOL SACHET
if(isset ($_POST['movicolsachet_date_time'])){
  $_SESSION['movicolsachet_date_time'] = $_POST['movicolsachet_date_time'];
}

if(isset ($_POST['movicolsachet_no_of_tablets'])){
  $_SESSION['movicolsachet_no_of_tablets'] = $_POST['movicolsachet_no_of_tablets'];
}


//LANSOPRAZOLE
if(isset ($_POST['lansoprazole_date_time'])){
  $_SESSION['lansoprazolen_date_time'] = $_POST['lansoprazole_date_time'];
}

if(isset ($_POST['lansoprazole_no_of_tablets'])){
  $_SESSION['lansoprazole_no_of_tablets'] = $_POST['lansoprazole_no_of_tablets'];
}


//OMEPRAZOLE
if(isset ($_POST['omeprazole_date_time'])){
  $_SESSION['omeprazole_date_time'] = $_POST['omeprazole_date_time'];
}

if(isset ($_POST['omeprazole_no_of_tablets'])){
  $_SESSION['omeprazole_no_of_tablets'] = $_POST['omeprazole_no_of_tablets'];
}



//GAVISON
if(isset ($_POST['gavison_date_time'])){
  $_SESSION['gavison_date_time'] = $_POST['gavison_date_time'];
}

if(isset ($_POST['gavison_no_of_tablets'])){
  $_SESSION['gavison_no_of_tablets'] = $_POST['gavison_no_of_tablets'];
}



//ORAL MORPHINE
if(isset ($_POST['oralmorphine_date_time'])){
  $_SESSION['oralmorphine_date_time'] = $_POST['oralmorphine_date_time'];
}

if(isset ($_POST['oralmorphine_no_of_tablets'])){
  $_SESSION['oralmorphine_no_of_tablets'] = $_POST['oralmorphine_no_of_tablets'];
}



//MORPHINE TABLETS
if(isset ($_POST['morphinetablets_date_time'])){
  $_SESSION['morphinetablets_date_time'] = $_POST['morphinetablets_date_time'];
}

if(isset ($_POST['morphinetablets_no_of_tablets'])){
  $_SESSION['morphinetablets_no_of_tablets'] = $_POST['morphinetablets_no_of_tablets'];
}


//PARACETAMOL
if(isset ($_POST['paracetamol_date_time'])){
  $_SESSION['paracetamol_date_time'] = $_POST['paracetamol_date_time'];
}

if(isset ($_POST['paracetamol_no_of_tablets'])){
  $_SESSION['paracetamol_no_of_tablets'] = $_POST['paracetamol_no_of_tablets'];
}



//CYCLIZINE
if(isset ($_POST['cyclizine_date_time'])){
  $_SESSION['cyclizine_date_time'] = $_POST['cyclizine_date_time'];
}

if(isset ($_POST['cyclizine_no_of_tablets'])){
  $_SESSION['cyclizine_no_of_tablets'] = $_POST['cyclizine_no_of_tablets'];
}



//DEXAMETHAZONE
if(isset ($_POST['dexamethazone_date_time'])){
  $_SESSION['dexamethazone_date_time'] = $_POST['dexamethazone_date_time'];
}

if(isset ($_POST['dexamethazone_no_of_tablets'])){
  $_SESSION['dexamethazone_no_of_tablets'] = $_POST['dexamethazone_no_of_tablets'];
}





//===========================================================================================
function printSymps ($symps)
{
  //print_r($symps);
  $count = count($symps);

  $i = 0;
  //$count = 0;
  $keys = array_keys($symps);

  for($i = 0; $i < $count-1;)
  {
    $name = $keys[$i];
    if($name == "pain")
    {
      $i++;
      continue;
    }

    echo('<div class="row" style="margin-bottom: 20px; text-align: center" >');
    for($j = 0; $j < 2; $j++ )
    {
      $name = $keys[$i++];
      //$severity = $symps[$name];
      echo getSympButton($name);
    }

    echo "</div>";
  }
}

function getSympButton($name)
{
  $btnType = "default";


  $str = "<div class='col-xs-6'>".
      "<div class='btn btn-lg btn-block btn-$btnType' id='$name' onclick='btnClick(\"$name\")'>$name</div>".
      "</div>";

  //echo $str;
  return $str;

}

//===========================================================================================================
?>


<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../../favicon.ico">
  <link rel="stylesheet" type="text/css" href="./jquery.datetimepicker.css"/>

  <title>Navbar Template for Bootstrap</title>

  <!-- Bootstrap core CSS -->
  <link href="dist/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="dist/css/navbar.css" rel="stylesheet">
  <link href="dist/css/jquery.datetimepicker.css" rel="stylesheet" TYPE="TEXT/CSS">


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>


<div class="container">

  <!-- Static navbar -->
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <a class="navbar-brand" href="prototype/homescreen.html">Back to Home Page</a>
      <!--<button class="navbar-btn btn btn-success pull-right" style="margin-left: 20px" onclick="done()">Done</button>-->
    </div><!--/.container-fluid -->
  </nav>

  <ol class="breadcrumb">
    <li><a href="prototype/homescreen.html">Home</a></li>

    <li class="active">Medication</li>

  </ol>

<body>

	<head>
		<!--<link rel="stylesheet" type="text/css" href="stylesheet.css">-->
	</head>

<article>
  <header>

    <h1><center>Medication History</center></h1>
    <h2><center>What medication are you taking?</center></h2>

  </header>

<center>

  <div class="row">
    <div class="col-lg-6">
      <a href="MedLogEntry.php?med=chemotherapy" class="allsymptoms"><button class="btn btn-lg btn-default btn-block">Chemotherapy</button></a>
    </div>
    <div class="col-lg-6">
      <a href="MedLogEntry.php?med=antisickness" class="allsymptoms"><button class="btn btn-lg btn-default btn-block">Anti-sickness</button></a>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-6">
      <a href="MedLogEntry.php?med=bowel" class="allsymptoms"><button class="btn btn-lg btn-default btn-block">Bowel</button></a>
    </div>
    <div class="col-lg-6">
      <a href="MedLogEntry.php?med=capecitabine" class="allsymptoms"><button class="btn btn-lg btn-default btn-block">Capecitabine</button></a>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-6">
      <a href="MedLogEntry.php?med=metoclopromide" class="allsymptoms"><button class="btn btn-lg btn-default btn-block">Metoclopromide</button></a>

    </div>
    <div class="col-lg-6">
      <a href="MedLogEntry.php?med=domperidone" class="allsymptoms"><button class="btn btn-lg btn-default btn-block">Domperidone</button></a>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-6">
      <a href="MedLogEntry.php?med=ondansetron" class="allsymptoms"><button class="btn btn-lg btn-default btn-block">Ondansetron</button></a>

    </div>
    <div class="col-lg-6">
      <a href="MedLogEntry.php?med=movicol" class="allsymptoms"><button class="btn btn-lg btn-default btn-block">Movicol Sachet</button></a>
    </div>
  </div>


  <div class="row">
    <div class="col-lg-6">
      <a href="MedLogEntry.php?med=lansoprazole" class="allsymptoms"><button class="btn btn-lg btn-default btn-block">Lansoprazole</button></a>

    </div>
    <div class="col-lg-6">
      <a href="MedLogEntry.php?med=omeprazole" class="allsymptoms"><button class="btn btn-lg btn-default btn-block">Omeprazole</button></a>
    </div>
  </div>


  <div class="row">
    <div class="col-lg-6">
      <a href="MedLogEntry.php?med=gavison" class="allsymptoms"><button class="btn btn-lg btn-default btn-block">Gavison</button></a>

    </div>
    <div class="col-lg-6">
      <a href="MedLogEntry.php?med=oral_morphine" class="allsymptoms"><button class="btn btn-lg btn-default btn-block">Oral morphine</button></a>
    </div>
  </div>


  <div class="row">
    <div class="col-lg-6">
      <a href="MedLogEntry.php?med=morphine_tablets" class="allsymptoms"><button class="btn btn-lg btn-default btn-block">Morphine tablets</button></a>

    </div>
    <div class="col-lg-6">
      <a href="MedLogEntry.php?med=paracetamol" class="allsymptoms"><button class="btn btn-lg btn-default btn-block">Paracetamol</button></a>
    </div>
  </div>


  <div class="row">
    <div class="col-lg-6">
      <a href="MedLogEntry.php?med=cyclizine" class="allsymptoms"><button class="btn btn-lg btn-default btn-block">Cyclizine</button></a>

    </div>
    <div class="col-lg-6">
      <a href="MedLogEntry.php?med=dexamethazone" class="allsymptoms"><button class="btn btn-lg btn-default btn-block">Dexamethazone</button></a>
    </div>
  </div>






   <br>
  <br>
	<a style="float" href="SaveMedData.php" class="homesymptom"><button class="btn btn-lg btn-primary btn-block">Submit</button></a>

</center>


<footer>

	<p>
<center>
	From Guy's and St Thomas' Hospital 
</center>

	 <div style="float: right;">

       <a href="apphelp.html" class="btn"><button class="btn btn-lg btn-warning">App Info</button></a>
	 </div>
</p>

</footer>

<script>
  function done()
  {
    alert("medicine history saved!");
  }
</script>
</body>
</html>

<?php
unset ( $_POST );
?>


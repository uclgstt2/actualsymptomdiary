<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Navbar Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="dist/css/navbar.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

	

<div class="container">

    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Symptom Diary</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">

                </ul>
                <ul class="nav navbar-nav navbar-right">

                </ul>
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>

    	<div>

    		<center> <label> Please enter your details and choose a 4 digit pin </label> </center>

    	</div>

    	<br>

    <!-- Main component for a primary marketing message or call to action -->
    <form >

    	
        <label>Name</label>
        <div class="form-group">
            <input id="pat_name" type="text" class="form-control" name="pat_name" placeholder="" required="true" >
        </div>
        <label>Age</label>
        <div class = "form-group">
            <input id="pat_age" type="text" class="form-control" name="pat_age" placeholder="" required="true" >
        </div>
		<label>Address</label>
        <div class="form-group">
            <input id="pat_address" type="text" class="form-control" name="pat_address" placeholder="" required="true" >
        </div>
        <label>PIN</label>
        <div class = "form-group">
            <input id="pat_pin" type="text" class="form-control" name="pat_pin" placeholder="4 Digit Number" required="true" >
        </div>
        <label>Do you consent for your data to be used in research?</label>
        
        <div class="form-group">
        <select class="form-control" name="pat_consent" type="select" id="pat_consent">
	    <option value="Yes">Yes</option>
	    <option value="No">No</option>
		</select>
        </div>
       
        <button type="button" class="btn btn-primary" onclick="Makeaccount()">Login</button>
        
    </form>

</div> <!-- /container -->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="dist/js/bootstrap.min.js"></script>

<script>
    function Makeaccount()
    {
        var name = $("#pat_name").val();
        var age = $("#pat_age").val();
        var address = $("#pat_address").val();
        var pin = $("#pat_pin").val();
 		var consent = $("#pat_consent").val();

      	var datastring = "pat_name="+name+"&pat_age="+age+"&pat_address="+address+"&pat_pin="+pin+"&pat_consent="+consent;


        $.post("makeaccount.php", datastring,function(data)
        {

            

            if(data == "success"){
                window.location.href = "prototype/homescreen.html";
            }
            else
            {

                
                $('.success').fadeIn(200).show().delay(2000).fadeOut(200);
                setTimeout(function() {
                    location.reload();
                },3000);
            }



        }).fail(function(){
            
        });
    }

    function show()
    {
        $('#yestext1').fadeIn(200).show();
        $('#yestext2').fadeIn(200).show();
    }
</script>

</body>
</html>
